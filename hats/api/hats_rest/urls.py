from django.urls import path
from .views import get_hats_detail, get_hats_list

urlpatterns = [
    path("hats/", get_hats_list, name="get_hats_list"),
    path("hats/<int:pk>/", get_hats_detail, name="get_hats_detail"),
]
