# Generated by Django 4.0.3 on 2022-12-03 23:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_import_href'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='location', to='hats_rest.locationvo'),
        ),
    ]
