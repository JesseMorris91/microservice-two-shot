# Generated by Django 4.0.3 on 2022-12-03 02:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='href',
            field=models.CharField(default=None, max_length=200),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bins', to='shoes_rest.binvo'),
        ),
    ]
