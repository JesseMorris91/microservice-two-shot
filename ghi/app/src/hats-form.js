import React from 'react';

class HatsForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        fabric: '',
      styleName: '',
      color: '',
      pictureUrl: '',
      locations: []
    };
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);


  }

  handleFabricChange(event) {
    const value = event.target.value;
    this.setState({fabric: value})
  }

  handleStyleNameChange(event) {
    const value = event.target.value;
    this.setState({styleName: value})
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({pictureUrl: value})
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({location: value})
  }

  async handleSubmit (event){
    event.preventDefault();
    const data = {...this.state};
    data.style_name = data.styleName;
    data.picture_url = data.pictureUrl;
    data.location = `/api/locations/${data.location}/`
    delete data.styleName;
    delete data.pictureUrl;
    delete data.locations;
    console.log(data)

    const hatsUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatsUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      const cleared = {
        fabric: '',
        styleName: '',
        color: '',
        pictureUrl: '',
        location: ''
      };
      this.setState(cleared);
    }
    console.log(response.body)
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({locations:data.locations});
    }
  }

  render() {
    return (
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a New Hat</h1>
          <form id="create-hat-form" onSubmit={this.handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="fabric" required type="text" id="fabric" name="fabric" className="form-control"/>
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="Style Name" required type="text" id="style_name" name="style_name" className="form-control"/>
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={this.handleColorChange} value={this.state.color} placeholder="color" required type="text" id="color" name="color" className="form-control"/>
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="Picture url" required type="url" id="picture_url" name="picture_url" className="form-control"/>
              <label htmlFor="picture_url">https://image.com </label>
            </div>
            <div className="mb-3">
              <select onChange={this.handleLocationChange} value={this.state.location} required id="location" name="location" className="form-select">
                <option value="">Choose a location</option>
                {this.state.locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.closet_name}
                        </option>
                    )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    );
  }
}

export default HatsForm;
