import React from "react";

class ShoesForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {bins: []};
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    async handleSubmit(event) {
        event.preventDefault();

        const data = {...this.state};
        data.model_name = data.modelName;
        data.picture_url = data.pictureUrl;
        data.bin = `/api/bins/${data.bin}/`
        delete data.modelName;
        delete data.pictureUrl;
        delete data.bins;
        console.log(data);

        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
             },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                pictureUrl: '',
                bin: '',
            };
            this.setState(cleared);
        }

    }


    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value});
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({modelName: value});
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value});
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({pictureUrl: value});
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value});
    }


    async componentDidMount() {

        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins});

        }

    }


   render() {
    return (

        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Shoe</h1>
              <form onSubmit={this.handleSubmit} id="shoes-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleModelNameChange} type="text" placeholder="Model Name" required name="starts" id="starts" className="form-control" />
                  <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleColorChange} type="text" placeholder="Color" required name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handlePictureUrlChange} placeholder="Picture Url" type="text" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="picture_url">Image</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleBinChange} id="bin" name="bin" className="form-select">
                    <option value="">Closet Name</option>
                    {this.state.bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button onChange={this.handleSubmit} className="btn btn-outline-info btn-md center-text">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>


    )
   }
}

export default ShoesForm;
