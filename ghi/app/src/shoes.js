import React from 'react';


class ShoesList extends React.Component {
    constructor() {
        super()
        this.state = {
            "shoes": []
        }
        this.delete = this.delete.bind(this)
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/'
        let response = await fetch(url)

        if (response.ok) {
            let data = await response.json()
            this.setState({'shoes': data.shoes})

        }

    }

    async delete(shoe) {
        const url = `http://localhost:8080/api/shoes/${shoe}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const deleteShoe = await response.json();
            window.location.reload();
            console.log(deleteShoe);

        }


    }


    render () {
        return(

  <div className='table-responsive pt-5'>
    <div className="col text-center">
    <a href='/shoes/create'>
     <button type="button" className="btn btn-outline-info btn-lg" >Add New Shoe</button>
     </a>
      </div>
        <div className='table-responsive pt-5'>
             <table className="table table-fixed" >
               <thead>
                  <tr>
                    <th scope="col" className="col-2">Manufacturer</th>
                    <th scope="col" className="col-2">Model Name</th>
                    <th scope="col" className="col-2">Color</th>
                    <th scope="col" className="col-2">Image</th>
                    <th scope="col" className="col-2">Bin</th>
                    <th scope="col" className="col-2">Actions</th>
                  </tr>
               </thead>
            <tbody>
            {this.state.shoes.map(shoe => {
                return (

                  <tr key={shoe.id}>
                    <td  className='col-2'>{shoe.manufacturer}</td>
                    <td  className="col-2">{shoe.model_name}</td>
                    <td  className="col-2">{shoe.color}</td>
                    <td  className="col-2">
                        <img src={shoe.picture_url} alt={shoe.href} height='100px' width='100px' />
                    </td>
                    <td className="col-2">Closet Name: {shoe.bin.closet_name} Number: {shoe.bin.bin_number} Size: {shoe.bin.bin_size}</td>
                    <td className="col-2">

                        <button className='btn btn-primary' onClick={() =>
                            this.delete(shoe.id)
                        }>Delete</button>

                    </td>
                  </tr>
                );
            })}
          </tbody>
        </table>
        <div>
      </div>
    </div>
  </div>


        );


    }
}

export default ShoesList;
